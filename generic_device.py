"""A generic serial device class and utilities."""

from serial import Serial

__author__ = 'Thomas Harrison'
__email__ = 'twh2898@vt.edu'
__license__ = 'MIT'
__version__ = '0.0.1'


CR = b'\r'
LF = b'\n'
CRLF = CR+LF


class InitializeError(RuntimeError):
    """Initialization of a Device failed for some reason."""

    def __init__(self, port, baud):
        """Create a new InitializeError."""
        super().__init__('Device {}:{} failed to initialize'.format(port, baud))


class Device:
    """A generic serial device."""

    _com = None
    """Internal reference to the serial port."""

    _timeouts = None
    """Internal stack for timeouts push and pop."""

    def __enter__(self):
        """Enter routine for context management."""
        return self

    def __exit__(self, *exc):
        """Exit routine for context management. Close the port if open."""
        self.close()

    def configure(self, com: Serial) -> bool:
        """
        Is called after the serial port is opened.

        Override this method for device specific setup after opening the serial
        port.
        """
        raise NotImplementedError('Device.configure has not been implemented')

    def open(self, port: str, baud: int, timeout: float):
        """
        Open the serial port.

        Open the serial port with the provided port, baud and timeout. Calls
        self.configure(com) for device specific setup after flushing input and
        output.

        Parameters:
          - port: device name
          - baud: serial baudrate
          - timeout: timeout for read operations

        Exceptions:
          - InitializeError: if the derived `configure` method returns `False`
        """
        self._com = Serial(port, baud, timeout=timeout)
        self.flush()
        if not self.configure(self._com):
            self.close()
            raise InitializeError(port, baud)

    def close(self):
        """Close the serial port."""
        if self._com.is_open:
            self._com.close()

    def flush(self):
        """Try to write all output data, then clear output and input buffers."""
        self._com.flush()
        self._com.flushOutput()
        self._com.flushInput()

    @property
    def timeout(self):
        """Get the serial port timeout."""
        return self._com.timeout

    @timeout.setter
    def timeout(self, timeout):
        """Set the serial port timeout."""
        self._com.timeout = timeout

    def push_timout(self, timeout: float):
        """Push the current timeout and set to `timeout`."""
        if self._timeouts is None:
            self._timeouts = []
        self._timeouts.append(self.timeout)
        self.timeout = timeout

    def pop_timeout(self):
        """Pop the timeout from the stack (ie. set to last)."""
        if self._timeouts is not None and len(self._timeouts) > 0:
            self.timeout = self._timeouts.pop()

    def write(self, data: bytes) -> int:
        """
        Output the given byte string over the serial port.

        Call `serial.Serial.write` internally.
        """
        return self._com.write(data)

    def read_until(self, expected: bytes = b'\n', size=None) -> bytes:
        """
        Read until an expected sequence is found.

        Call `serial.Serial.read_until` internally.
        """
        return self._com.read_until(expected, size)

    def read_until_timeout(self, expected: bytes = b'\n', size=None, timeout=None) -> bytes:
        r"""
        Call `serial.Serial.read_until` with a temporary read timeout.

        This method uses `serial.Serial.read_until()` internally. The read timeout
        will be temporarily changed to the `timeout` parameter, and restored after
        the read operation is complete.

        Parameters:
          - expected: byte string to search for, default is `\n`
          - size: number of bytes to read
          - timeout: temporary read timeout

        See `serial.Serial` for possible values of timeout.
        """
        self.push_timout(timeout)
        data = self._com.read_until(expected, size)
        self.pop_timeout()
        return data

    def read_packet(self, begin: bytes = None, end: bytes = CRLF, strip: bool = True):
        """
        Read a packet based on an expected start and end sequence.

        If `begin` is `None`, the packet will begin with the next byte read.

        If `strip` is `True`, the begin and end sequences will be stripped from
        the packet before it is returned.

        Parameters:
          - begin: Optional[bytes] the byte string that is the start of a packet
          - end: bytes the byte string that is the end of a packet
          - strip: bool should the `begin` and `end` byte strings be striped form
            the packet

        Returns:
          the packet or `None` if `begin` or `end` were not found
        """
        if begin is not None:
            if not self.read_until(begin).endswith(begin):
                return None

        packet = (begin or b'') + self.read_until(end)

        if packet.endswith(end):
            if strip:
                packet = packet.lstrip(begin).rstrip(end)
            return packet
        else:
            return None
