# generic_device

A generic serial device class and utilities.

## Install

```sh
git clone https://gitlab.com/twh2898/generic_device
cd generic_device
pip install .
```

### Development Install

Replace the pip command in the above code with the following (note the `-e`).

```sh
pip install -e .
```

## Building

```sh
git clone https://gitlab.com/twh2898/generic_device
cd generic_device
pip install build
python -m build
```

## License

generic_device uses the [MIT](LICENSE) License.
